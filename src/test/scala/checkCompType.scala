import org.scalatest.FunSuite
import main._
import main.Type._

class CheckCompTypeSuite extends FunSuite {
  test("return one : F ()") {
    assert(checkCompType(Map(), Return(One()), ReturnT(OneT())))
  }
  test("return one to x. return x : F ()") {
    val m = To(Return(One()), 0, Return(Var(0)))
    assert(checkCompType(Map(), m, ReturnT(OneT())))
  }
  test("one ‘ λx. return x : F ()") {
    val m = ApplyArrow(One(), Lam(0, Return(Var(0))))
    assert(checkCompType(Map(), m, ReturnT(OneT())))
  }
  test("let one be x. return x : F ()") {
    val m = Let(One(), 0, Return(Var(0)))
    assert(checkCompType(Map(), m, ReturnT(OneT())))
  }
  test("pm (true, one) as { (true, x). return x, (false, x). return (false, x) } : F ()") {
    val m: Compute[Boolean] = MatchVariant(Variant(true, One()), List((true, 0, Return(Var(0))), (false, 0, Return(Variant(false, Var(0))))))
    assert(checkCompType(Map(), m, ReturnT(OneT())))
  }
  test("pm (false, one) as { (true, x). return x, (false, x). return (false, x) } : ∑ (false ↦ ())") {
    val m: Compute[Boolean] = MatchVariant(Variant(false, One()), List((true, 0, Return(Var(0))), (false, 0, Return(Variant(false, Var(0))))))
    assert(checkCompType(Map(), m, ReturnT(Sigma(Map(false -> OneT[Boolean]())))))
  }
  ignore("return thunk λx. return x : F (U (() → F ()))") {
    val m: Compute[Boolean] = Return(Thunk(Lam(0, Return(Var(0)))))
    assert(checkCompType(Map(), m, ReturnT(ThunkT(Arrow(OneT(), ReturnT(OneT()))))))
  }
  ignore("pm (one, thunk λx. return x) as (x, y). return (y, x) : F (U (() → F ()) × ())") {
    val id: Value[Boolean] = Thunk(Lam(0, Return(Var(0))))
    val m: Compute[Boolean] = MatchProd(Prod(One(), id), 0, 1, Return(Prod(Var(1), Var(0))))
    val idT: ValType[Boolean] = ThunkT(Arrow(OneT(), ReturnT(OneT())))
    assert(checkCompType(Map(), m, ReturnT(ProdT(idT, OneT()))))
  }
}

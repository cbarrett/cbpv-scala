/*
import org.scalacheck._
import org.scalacheck.Gen._
import Main._

object ValueGen {
  val genIndex = arbBool

  val genThunk = for {
    (m, mT) <- genComp
  } yield (Thunk(m), ThunkT(m))

  val genVariant = for {
    i <- genIndex
    (v, vT) <- genValue
  } yield (Variant(i, v), Sigma(j => if (i == j) vT else ???))

  val genProd = for {
    (v1, v1T) <- genValue
    (v2, v2T) <- genValue
  } yield (Prod(v1, v2), ProdT(v1T, v2T))

  val genReturn = for {
    (v, vT) <- genValue
  } yield (Return(v), ReturnT(vT))

  val genRecord = for {
    map <- genRecordMap
  } yield (Record(map), Pi(i => map.getOrElse(i, ???)))

  /*
  def genLam(f: (Value[Boolean]) => Gen[Value[Boolean]]): Gen[Value[Boolean]] = mfixGen((body, n) => for {
    body2 <- f(Var(n))
    n2 <- succIdent(maxBV(body))
  } yield (body2, n2))
  */
  val genValue = oneOf(genThunk, genVariant, genProd, ???)
  val genTermComp = oneOf(genReturn, genRecord, /* genLam(genValue) */ ???)
  val genComp = oneOf(getTermComp, ???)

  // Axelsson and Claessen (2013)
  val botIdent = "O"
  def succIdent(ident: String): String =
    new String(ident.toInt + 1)
  def maxIdent(ident1: String, ident2: String): String =
    new String(ident1.toInt max ident2.toInt)
  def maxBV[I](v: Value[I]): String =
    v match {
      case One() => botIdent
      case Var(_) => botIdent
      case Thunk(m) => maxBV(m)
      case Variant(_, v) => maxBV(v)
      case Prod(v1, v2) => maxIdent(maxBV(v1), maxBV(v2))
    }
  def maxBV[I](m: Compute[I]): String =
    m match {
      case Return(v) => maxBV(v)
      case Record(map) => ???
      case Lam(ident, _) => ident
      case Let(v, ident, _) => maxIdent(maxBV(v), ident)
      case To(m, ident, _) => maxIdent(maxBV(m), ident)
      case Force(v) => maxBV(v)
      case MatchVariant(v, cases) => ???
      case MatchProd(v, id1, id2, _) => maxIdent(maxBV(v), maxIdent(id1, id2))
      case ApplyPi(i, m) => maxBV(m)
      case ApplyArrow(v, m) => maxIdent(maxBV(v), maxBV(m))
    }
}
 */

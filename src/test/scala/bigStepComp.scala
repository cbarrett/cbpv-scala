import org.scalatest.FunSuite
import main._
import main.BigStep._

class BigStepCompSuite extends FunSuite {
  test("return one ⇓ return one") {
    assert(bigStepComp(Return(One())) == Return(One()))
  }
  test("return one to x. return x ⇓ return one") {
    val m: Compute[Boolean] = To(Return(One()), 0, Return(Var(0)))
    assert(bigStepComp(m) == Return(One()))
  }
  test("one ‘ λx. return x ⇓ return one") {
    val m: Compute[Boolean] = ApplyArrow(One(), Lam(0, Return(Var(0))))
    assert(bigStepComp(m) == Return(One()))
  }
  test("let one be x. return x ⇓ return one") {
    val m: Compute[Boolean] = Let(One(), 0, Return(Var(0)))
    assert(bigStepComp(m) == Return(One()))
  }
  test("pm (true, one) as { (true, x). return x, (false, x). return (false, x) } ⇓ return one") {
    val m: Compute[Boolean] = MatchVariant(Variant(true, One()), List((true, 0, Return(Var(0))), (false, 0, Return(Variant(false, Var(0))))))
    assert(bigStepComp(m) == Return(One()))
  }
  test("pm (false, one) as { (true, x). return x, (false, x). return (false, x) } ⇓ return (false, one)") {
    val m: Compute[Boolean] = MatchVariant(Variant(false, One()), List((true, 0, Return(Var(0))), (false, 0, Return(Variant(false, Var(0))))))
    assert(bigStepComp(m) == Return(Variant(false, One())))
  }
  test("pm (one, thunk λx. return x) as (x, y). return (y, x) ⇓ (thunk λx. return x, one)") {
    val id: Value[Boolean] = Thunk(Lam(0, Return(Var(0))))
    val m: Compute[Boolean] = MatchProd(Prod(One(), id), 0, 1, Return(Prod(Var(1), Var(0))))
    assert(bigStepComp(m) == Return(Prod(id, One())))
  }
  test("force thunk return one ⇓ return one") {
    val n: Compute[Boolean] = Return(One())
    val m: Compute[Boolean] = Force(Thunk(n))
    assert(bigStepComp(m) == n)
  }
  test("apply pi") {
    val t: Compute[Boolean] = Return(One())
    val f: Compute[Boolean] = Lam(0, Return(One()))
    val m = Record(Map(true -> t, false -> f))
    assert(bigStepComp(ApplyPi(true, m)) == t)
    assert(bigStepComp(ApplyPi(false, m)) == f)
  }
}

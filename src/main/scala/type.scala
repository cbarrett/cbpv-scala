package main

package object Type {

  sealed trait ValType[I]
  case class OneT[I]() extends ValType[I]
  case class ProdT[I](p1: ValType[I], p2: ValType[I]) extends ValType[I]
  case class Sigma[I](map: Map[I, ValType[I]]) extends ValType[I]
  case class ThunkT[I](typ: CompType[I]) extends ValType[I]

  sealed trait CompType[I]
  case class ReturnT[I](typ: ValType[I]) extends CompType[I]
  case class Pi[I](map: Map[I, CompType[I]]) extends CompType[I]
  case class Arrow[I](dom: ValType[I], cdm: CompType[I]) extends CompType[I]

  type Context[I] = Map[Int, ValType[I]]

  def synthValType[I](c: Context[I], v: Value[I]): ValType[I] = v match {
    case One() => OneT()
    case Var(ident) => c.getOrElse(ident, ???)
    case Thunk(m) => ThunkT(synthCompType(c, m))
    case Variant(index, v) => Sigma(Map(index -> synthValType(c, v)))
    case Prod(v1, v2) => ProdT(synthValType(c, v1), synthValType(c, v2))
  }

  def synthTermCompType[I](c: Context[I], t: TerminalCompute[I]): CompType[I] = t match {
    case Return(v) => ReturnT(synthValType(c, v))
    case Record(map) => Pi(map.map({ case (i, m) => i -> synthCompType(c, m) }))
    case Lam(ident, m) => Arrow(c.getOrElse(ident, ???), synthCompType(c, m))
  }

  def synthCompType[I](c: Context[I], m: Compute[I]): CompType[I] =
    synthTermCompType(c, BigStep.bigStepComp(m))

  def checkCompType[I](c: Context[I], m: Compute[I], typ: CompType[I]): Boolean =
    m match {
      case Return(v) => typ match {
        case ReturnT(typ2) => typ2 == synthValType(c, v)
        case _ => false
      }
      case Record(map) => typ match {
        case Pi(typ) => map.foldLeft(true) {
          case (ok, (i, m)) => ok && checkCompType(c, m, typ(i))
        }
        case _ => false
      }
      case Lam(ident, m) => typ match {
        case Arrow(dom, cdm) => checkCompType(c.updated(ident, dom), m, cdm)
        case _ => false
      }
      case Let(v, ident, m) =>
        checkCompType(c.updated(ident, synthValType(c, v)), m, typ)
      case To(m, ident, n) =>
        synthCompType(c, m) match {
          case ReturnT(vT) => checkCompType(c.updated(ident, vT), n, typ)
          case _ => false
        }
      case Force(v) => synthValType(c, v) match {
        case ThunkT(typ2) => typ == typ2
        case _ => false
      }
      case MatchVariant(v, cases) => synthValType(c, v) match {
        case Sigma(typ2) => cases.foldRight(false) {
          case ((index, ident, m), ok) =>
            if (typ2.contains(index))
              checkCompType(c.updated(ident, typ2(index)), m, typ) || ok
            else ok
        }
        case _ => false
      }
      case MatchProd(v, ident1, ident2, m) => synthValType(c, v) match {
        case ProdT(t1, t2) =>
          checkCompType(c.updated(ident1, t1).updated(ident2, t2), m, typ)
        case _ => false
      }
      case ApplyPi(index, m) =>
        synthCompType(c, m) match {
          case Pi(typ2) => typ == typ2(index)
          case _ => false
        }
      case ApplyArrow(v, m) =>
        checkCompType(c, m, Arrow(synthValType(c, v), typ))
    }
}

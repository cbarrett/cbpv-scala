package main

package object BigStep {
  class Subst[I](val map: Map[Int, Value[I]] = Map[Int, Value[I]]()) {
    def apply(v: Value[I]): Value[I] = v match {
      case Var(ident) => this.map.getOrElse(ident, v)
      case Variant(index, v) => Variant(index, this(v))
      case Prod(v1, v2) => Prod(this(v1), this(v2))
      case _ => v
    }
    def updated(k: Int, v: Value[I]): Subst[I] =
      new Subst(this.map.updated(k, this(v)))
  }

  def bigStepComp[I](m: Compute[I]): TerminalCompute[I] =
    bigStepComp(new Subst[I](), m)
  def bigStepComp[I](s: Subst[I], m: Compute[I]): TerminalCompute[I] =
    m match {
      case Return(v) => Return(s(v))
      case t: Record[I] => t
      case t: Lam[I] => t
      case Let(v, ident, m) => bigStepComp(s.updated(ident, v), m)
      case To(m, ident, n) => bigStepComp(s, m) match {
        case Return(v) => bigStepComp(s.updated(ident, v), n)
        case _ => ???
      }
      case Force(v) => s(v) match {
        case Thunk(m) => bigStepComp(s, m)
        case _ => ???
      }
      case MatchVariant(v, cases) => s(v) match {
        case Variant(index, v) =>
          val (_, ident, m) = cases.find(kase => kase._1 == index).getOrElse(???)
          bigStepComp(s.updated(ident, v), m)
        case _ => ???
      }
      case MatchProd(v, ident1, ident2, m) => s(v) match {
        case Prod(v1, v2) => bigStepComp(s.updated(ident1, v1).updated(ident2, v2), m)
        case _ => ???
      }
      case ApplyPi(index, m) => bigStepComp(s, m) match {
        case Record(map) => bigStepComp(s, map(index))
        case _ => ???
      }
      case ApplyArrow(v, m) => bigStepComp(s, m) match {
        case Lam(ident, n) => bigStepComp(s.updated(ident, v), n)
        case _ => ???
      }
    }
}

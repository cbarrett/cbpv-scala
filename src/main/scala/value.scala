package main

sealed trait Value[I]
case class One[I]() extends Value[I]
case class Var[I](ident: Int) extends Value[I]
case class Thunk[I](m: Compute[I]) extends Value[I]
case class Variant[I](index: I, v: Value[I]) extends Value[I]
case class Prod[I](v1: Value[I], v2: Value[I]) extends Value[I]

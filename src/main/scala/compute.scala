package main

sealed trait Compute[I]
sealed trait TerminalCompute[I] extends Compute[I]
case class Return[I](v: Value[I]) extends TerminalCompute[I]
case class Record[I](map: Map[I, Compute[I]]) extends TerminalCompute[I]
case class Lam[I](ident: Int, m: Compute[I]) extends TerminalCompute[I]
case class Let[I](v: Value[I], ident: Int, m: Compute[I]) extends Compute[I]
case class To[I](m: Compute[I], ident: Int, n: Compute[I]) extends Compute[I]
case class Force[I](v: Value[I]) extends Compute[I]
case class MatchVariant[I](v: Value[I], cases: List[(I, Int, Compute[I])])
  extends Compute[I]
case class MatchProd[I](v: Value[I], id1: Int, id2: Int, m: Compute[I])
  extends Compute[I]
case class ApplyPi[I](index: I, m: Compute[I]) extends Compute[I]
case class ApplyArrow[I](v: Value[I], m: Compute[I]) extends Compute[I]
